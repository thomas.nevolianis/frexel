# frexel #

### What is this repository for? ###

This script is used to identify the atoms with the maximum displacement and can be used for anharmonic calculations.

For help run:  `frexel.py --h`
```
usage: frexel.py [-h] [--max_atoms_per_mode MAX_ATOMS_PER_MODE] log_file gjf_file label
```
```
This script is used to identify the atoms with the maximum displacement and can be used for anharmonic calculations.

positional arguments:
  log_file              log_file with the frequency calculations from Gaussian
  gjf_file              gjf file with which the frequency calculations are performed
  label                 provide a label which corresponds to a valid atom

optional arguments:
  -h, --help            show this help message and exit
  --max_atoms_per_mode MAX_ATOMS_PER_MODE
                        provide maximum atoms per mode (default=3)
