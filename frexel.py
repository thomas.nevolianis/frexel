import numpy as np
import pathlib
import argparse

# Argparse
parser = argparse.ArgumentParser(
    description='This script is used to identify the atoms with the maximum displacement and can be used for anharmonic calculations.')
parser.add_argument('log_file', type=str,
                    help='log_file with the frequency calculations from Gaussian')
parser.add_argument('gjf_file', type=str,
                    help='gjf file with which the frequency calculations are performed')
parser.add_argument('label', type=int,
                    help='provide a label which corresponds to a valid atom')
parser.add_argument('--max_atoms_per_mode', type=int, default=3,
                    help='provide maximum atoms per mode (default=3)')


args = parser.parse_args()


class Frexel:
    '''
    Calculates the modes in which a label (atom) is involved.
    :param label: label of an atom
    :param logfile: a gaussian output logfile
    :param gjf: a gaussian frequency calculations input file
    :param maximum_atoms_per_mode: maximum atoms to consider in each mode
    :return: frequencies
    :type: list
    '''

    def __init__(self, label:int, logfile: str, gjffile: str, maximum_atoms_per_mode: int=3):
        self.label = label
        self.logfile = logfile
        self.gjffile = gjffile
        self.maximum_atoms_per_mode = maximum_atoms_per_mode

    def get_natoms(self):
        '''
        Gets the number of atoms.
        :return: number of atoms
        :type: int
        '''
        atoms_tmp = ""
        search_atoms = "NAtoms="
        with open(self.logfile, "r") as f:
            for line in f:
                if search_atoms in line:
                    atoms_tmp = line.split()[1]
        return int(atoms_tmp)
     

    def get_freqs(self):
        '''
        Gets the frequencies.
        :return: frequencies
        :type: list
        '''
        freqs_tmp = []
        search_freqs = "Frequencies --"
        with open(self.logfile, "r") as f:
            for line in f:
                if search_freqs in line:  # searching for the Keyword of finding the freqs
                    freqs_tmp.append(float(line.split()[2]))
                    freqs_tmp.append(float(line.split()[3]))
                    freqs_tmp.append(float(line.split()[4]))
        return freqs_tmp

    def get_displacements(self):
        '''
        Gets the largest displacements in each mode.
        :return: displacements
        :type: list
        '''
        natoms = self.get_natoms()
        with open(self.logfile) as f:
            text = f.read()
        text = text.split('\n')

        lines = []
        s = '  Atom  AN      X      Y      Z        X      Y      Z        X      Y      Z'
        for n, line in enumerate(text):
            if s in line:
                lines.append(text[n+1:n+natoms+1])

        r = []
        for blocks in lines:
            for i in blocks:
                labels = []
                types = []
                x1,y1,z1 = [], [], []
                x2,y2,z2 = [], [], []
                x3,y3,z3 = [], [], []
                r1,r2,r3 = [], [], []

                for i in range(natoms):
                    labels.append(int(blocks[i].split()[0]))
                    types.append(int(blocks[i].split()[1]))
                    x1.append(float(blocks[i].split()[2]))
                    y1.append(float(blocks[i].split()[3]))
                    z1.append(float(blocks[i].split()[4]))
                    x2.append(float(blocks[i].split()[5]))
                    y2.append(float(blocks[i].split()[6]))
                    z2.append(float(blocks[i].split()[7]))
                    x3.append(float(blocks[i].split()[8]))
                    y3.append(float(blocks[i].split()[9]))
                    z3.append(float(blocks[i].split()[10]))

                for i,j,k in zip(x1,y1,z1):
                    r1.append(np.sqrt(i**2+j**2+k**2))
                
                for i,j,k in zip(x2,y2,z2):
                    r2.append(np.sqrt(i**2+j**2+k**2))

                for i,j,k in zip(x3,y3,z3):
                    r3.append(np.sqrt(i**2+j**2+k**2))

                label_to_r1 = {i:j for i,j in zip(labels,r1)}
                r1_to_label = {j:i for i,j in zip(labels,r1)}
                label_to_r2 = {i:j for i,j in zip(labels,r2)}
                r2_to_label = {j:i for i,j in zip(labels,r2)}
                label_to_r3 = {i:j for i,j in zip(labels,r3)}
                r3_to_label = {j:i for i,j in zip(labels,r3)}

                _r1_ = sorted([label_to_r1[i] for i in labels], reverse=True)
                r1_ = [r1_to_label[i] for i in _r1_][:self.maximum_atoms_per_mode]

                _r2_ = sorted([label_to_r2[i] for i in labels], reverse=True)
                r2_ = [r2_to_label[i] for i in _r2_][:self.maximum_atoms_per_mode]

                _r3_ = sorted([label_to_r3[i] for i in labels], reverse=True)
                r3_ = [r3_to_label[i] for i in _r3_][:self.maximum_atoms_per_mode]

                r.append(r1_)
                r.append(r2_)
                r.append(r3_)
        return r[0::natoms]


    def get_label_modes(self):
        '''
        Gets the modes for a given label.
        :return: label modes
        :type: list
        '''
        displs = self.get_displacements()
        label_modes = []

        for n,freq_labels in enumerate(displs):
            for freq_label in freq_labels:
                if freq_label==self.label:
                    label_modes.append(n+1)
        return label_modes

    def get_freqs_from_modes(self, modes):
        '''
        Gets the freqs from modes.
        :return: label modes
        :type: list
        '''
        all_freqs = self.get_freqs()
        all_modes = [i+1 for i in range(len(all_freqs))]
        from_modes_to_freqs = {j:i for i,j in zip(all_freqs, all_modes)}
        freqs = [from_modes_to_freqs[i] for i in modes]
        return freqs
    
    def create_anharmonic_gjf(self):
        '''
        Prepares a .gjf format file with the anharmonic modes based on a given file.
        Default "maximum_atoms_per_mode=3"
        :return: label modes
        :type: list
        '''
        modes = self.get_label_modes()
        modes = str(modes).replace("[","")
        modes = str(modes).replace("]","")
        modes = str(modes).replace(" ","")
        with open(self.gjffile, 'r') as gjf, open(f"{pathlib.Path(self.gjffile).stem}_anharm.gjf",'w') as gjf_anharm:
            for line in gjf:
                gjf_anharm.write(line.replace('freq', 'freq=(anharmonic,selectanharmonicmodes)'))
            gjf_anharm.write(line)
            gjf_anharm.write(f'modes={modes}\n')

        file = open(f"{pathlib.Path(self.gjffile).stem}_anharm.gjf", 'rb')
        pos = next = 0
        for line in file:
            pos = next # position of beginning of this line
            next += len(line) # compute position of beginning of next line
        file = open(f"{pathlib.Path(self.gjffile).stem}_anharm.gjf", 'ab')
        file.truncate(pos-1)

        with open(f"{pathlib.Path(self.gjffile).stem}_anharm.gjf",'a') as gjf_anharm:
            gjf_anharm.write(f'modes={modes}\n')
    
    def create_csv(self):
        '''
        Creates a csv file containing details regarding 
        :return: label modes
        :type: list
        '''
        with open("analysis.txt",'w') as f:
            f.write(f"label: {self.label}\n\n")
            for i in range(1,6):
                self.maximum_atoms_per_mode = i
                modes = self.get_label_modes()
                freqs = self.get_freqs_from_modes(modes)
                f.write(f"\nbest_atoms_per_mode: {self.maximum_atoms_per_mode}\n")
                f.write("\n".join("{} \t {}".format(x, y) for x, y in zip(modes, freqs)))
                f.write("\n")

LM = Frexel(label=args.label,
            logfile=args.log_file,
            gjffile=args.gjf_file,
            maximum_atoms_per_mode=args.max_atoms_per_mode
            )
LM.create_anharmonic_gjf()
LM.create_csv()
